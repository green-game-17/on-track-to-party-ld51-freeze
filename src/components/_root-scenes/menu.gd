extends Control


onready var main_block = $Main
onready var options_block = $Options
onready var start_button = $Main/StartButton
onready var options_button = $Main/OptionsButton
onready var back_button = $Options/BackButton
onready var blur_checkbox = $Options/CheckBox
onready var volume_slider = $Options/VolumeContainer/HSlider
onready var volume_label = $Options/VolumeContainer/Volume


func _ready():
	start_button.connect('pressed', self, '__on_start')
	options_button.connect('pressed', self, '__show_options')
	back_button.connect('pressed', self, '__back_to_menu')
	blur_checkbox.connect('pressed', self, '__set_blur')
	volume_slider.connect('value_changed', self, '__volume_changed')


func __on_start():
	SCENE.goto_scene('story')


func __show_options():
	main_block.hide()
	options_block.show()


func __back_to_menu():
	options_block.hide()
	main_block.show()


func __set_blur():
	STATE.set_blur(!STATE.blur_active)
	blur_checkbox.set_pressed(STATE.blur_active)


func __volume_changed(new_value):
	volume_label.set_text(str(new_value))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index('Master'), new_value - 80)
