extends Control


onready var images = [
	$CenterContainer/HBoxContainer/Image1,
	$CenterContainer/HBoxContainer/Image2,
	$CenterContainer/HBoxContainer/Image3,
]


var current_image_no = 0


func _ready():
	$Timer.connect('timeout', self, '_on_timeout')

	$ButtonContainer/Button.connect('pressed', self, '_on_skip')

	for img in images:
		img.modulate.a = 0.0


func _process(_delta):
	var curr_image = images[current_image_no]
	if curr_image.modulate.a < 0.99:
		curr_image.modulate.a += 0.1

	if current_image_no > 0 && current_image_no != 5:
		var prev_image = images[current_image_no - 1]
		if current_image_no == 9:
			prev_image = $"MazeOverlay2/CenterContainer/VBoxContainer/Label1"
		if prev_image.modulate.a > 0.4:
			prev_image.modulate.a -= 0.1


func _on_timeout():
	if current_image_no < images.size()-1:
		current_image_no += 1
	else:
		$ButtonContainer/Button.modulate.a = 1.0


func _on_skip():
	SCENE.goto_scene('world')
