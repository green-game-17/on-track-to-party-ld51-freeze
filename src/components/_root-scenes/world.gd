extends Node


onready var camera = $ViewportContainer/Viewport/Camera
onready var environment = $ViewportContainer/Viewport/Environment
onready var shader = $ViewportContainer.material


var is_driving = false


func _ready():
	EVENTS.connect('arrive_station', self, '__on_arrive')
	EVENTS.connect('depart_station', self, '__on_depart')

	camera.make_current()

	var camera_update = camera.update_speed_and_position(0.0)
	environment.update_for_camera(camera_update.x)
	


func _input(event):
	if event.is_action_pressed("ui_cancel"):
		__on_cancel()


func _physics_process(delta):
	if is_driving:
		var camera_update = camera.update_speed_and_position(delta)
		environment.update_for_camera(camera_update.x)
		if STATE.blur_active:
			shader.set_shader_param('trail_length', camera_update.trail_length)
		if camera_update.arrived:
			EVENTS.emit_signal('arrive_station')


func __on_arrive():
	is_driving = false


func __on_depart():
	is_driving = true

	camera.reset()
	var camera_update = camera.update_speed_and_position(0.0)
	environment.update_for_camera(camera_update.x)


func __on_cancel():
#	SCENE.goto_scene('menu')
	pass
