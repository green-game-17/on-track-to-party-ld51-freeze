extends Node

const STATION_AMOUNT := 100
const MIN_ROUNDTRIP_AMOUNT := 5
const MIN_DIST := 40
const MAX_DIST := 220

var MIN_DIST_SQUARED := pow(MIN_DIST, 2)
var MAX_DIST_SQUARED := pow(MAX_DIST, 2)
var rng : RandomNumberGenerator = RandomNumberGenerator.new()
var network : Dictionary = {
	0: {
		'pos': Vector2.ZERO,
		'connections': {
			'left': [],
			'right': []
		},
		'biome': null
	}
}
var noise: OpenSimplexNoise = OpenSimplexNoise.new()

func _ready():
	rng.randomize()

func generate_network():
	for i in range(1, STATION_AMOUNT):
		var current_station = {
			'pos': Vector2.ZERO,
			'connections': {
				'left': [],
				'right': []
			}
		}
		var ended = false
		while not ended:
			current_station.pos = Vector2(rng.randi_range(-500, 500), rng.randi_range(-500, 500))
			var cursta_pos = Vector2(current_station.pos.x, current_station.pos.y)
			var found_station := false
			for j in range(network.size()):
				var compare_station = network.get(j)
				var comsta_pos = Vector2(compare_station.pos.x, compare_station.pos.y)
				if cursta_pos.distance_squared_to(comsta_pos) < MIN_DIST_SQUARED:
					found_station = true
					break
			if found_station:
				continue
			else:
				ended = true
		network[i] = current_station
	
	for i in range(network.size()): # all stations
		var station = network.get(i)
		if not is_station_available(station):
			continue
		
		var found_stations = []
		for j in range(network.size()): # checking every other station
			var station_check = network.get(j)
			if (i == j or not is_station_available(station_check) or already_connected(station, j) or
				has_roundtrip(i, j)):
				continue
			
			var dist = station.pos.distance_squared_to(station_check.pos)
			if dist <= MAX_DIST_SQUARED:
				found_stations.append([j, dist])
		
		found_stations.sort_custom(CustomSorter, 'sort')
#		print(found_stations)
		for k in found_stations:
			if has_roundtrip(i, k[0]):
				continue
			var new_station = network.get(k[0])
			if can_connect(station.connections.left, new_station.connections.left):
				station.connections.left.append(k[0])
				for l in new_station.connections.left:
					if not station.connections.left.has(l):
						station.connections.left.append(l)
				new_station.connections.left.append(i)
			elif can_connect(station.connections.left, new_station.connections.right):
				station.connections.left.append(k[0])
				for l in new_station.connections.right:
					if not station.connections.left.has(l):
						station.connections.left.append(l)
				new_station.connections.right.append(i)
			elif can_connect(station.connections.right, new_station.connections.left):
				station.connections.right.append(k[0])
				for l in new_station.connections.left:
					if not station.connections.right.has(l):
						station.connections.right.append(l)
				new_station.connections.left.append(i)
			elif can_connect(station.connections.right, new_station.connections.right):
				station.connections.right.append(k[0])
				for l in new_station.connections.right:
					if not station.connections.right.has(l):
						station.connections.right.append(l)
				new_station.connections.right.append(i)
			else:
				continue
		
		for k in station.connections.left:
			var new_station = network.get(k)
			for l in station.connections.left:
				if k != l and not already_connected(new_station, l):
					if new_station.connections.left.has(i):
						new_station.connections.left.append(l)
					else:
						new_station.connections.right.append(l)
		for k in station.connections.right:
			var new_station = network.get(k)
			for l in station.connections.right:
				if k != l and not already_connected(new_station, l):
					if new_station.connections.left.has(i):
						new_station.connections.left.append(l)
					else:
						new_station.connections.right.append(l)


func generate_biomes():
	noise.seed = rng.randi()
	noise.octaves = 5
	noise.period = 150.0
	noise.persistence = 0.25
	
	for i in range(network.size()):
		var station = network.get(i)
		station.biome = noise.get_noise_2d(station.pos.x, station.pos.y) * 5


func is_station_available(station: Dictionary) -> bool:
	if station.connections.left.size() < 3 or station.connections.right.size() < 3:
		return true
	return false


func available_conns(station: Dictionary) -> int:
	return 6 - (station.connections.left.size() + station.connections.right.size())


func has_roundtrip(station_key: int, key: int) -> bool:
	var covered_points = []
	for i in MIN_ROUNDTRIP_AMOUNT:
		covered_points.append([])
		if i == 0:
			covered_points[i].append(key)
		else:
			for j in covered_points[i - 1]:
				var new_station = network.get(j)
				for k in new_station.connections.left:
					if not covered_points[i].has(k) and not covered_points[i - 1].has(k):
						covered_points[i].append(k)
				for k in new_station.connections.right:
					if not covered_points[i].has(k) and not covered_points[i - 1].has(k):
						covered_points[i].append(k)
#	print(covered_points)
	for layer in covered_points:
		if layer.has(station_key):
			return true
	return false


func already_connected(station: Dictionary, key: int) -> bool:
	return station.connections.left.has(key) or station.connections.right.has(key)


func can_connect(conn1: Array, conn2: Array) -> bool:
	var connections = conn1.duplicate()
	for i in conn2:
		if not connections.has(i):
			connections.append(i)
	return connections.size() < 3


class CustomSorter:
	static func sort(a: Array, b: Array, asc: bool = true):
		if asc:
			if a[1] < b[1]:
				return true
			return false
		if a[1] > b[1]:
			return true
		return false
