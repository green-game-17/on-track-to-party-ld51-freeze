extends Node

# Mutable, shared state data:

onready var rng = RandomNumberGenerator.new()

var blur_active = true

var _current_station_id = 0
var _target_station_id = 0
var _selected_path = 1
var _has_arrived_by_left_connection = false

var _train = ID.TRAIN_1
var _carriages = [
	{
		id = ID.CARRIAGE_CARGO_1
	},
	{
		id = ID.CARRIAGE_PASSENGERS_1
	},
]
var _wheels = ID.BIOME_PLAINS

var _inventory = [
]
var _passengers = 0
var _money = 10_000



func _ready():
	rng.randomize()

	EVENTS.connect('arrive_station', self, '__on_arrive_station')
	set_current_station(_current_station_id)


func set_blur(is_blur_active):
	blur_active = is_blur_active


func __on_arrive_station():
	_has_arrived_by_left_connection = _current_station_id in get_target_station().connections.left
	set_current_station(_target_station_id)


func set_current_station(id):
	_current_station_id = id
	set_selected_path(rng.randi_range(0, get_paths_count()-1))
	EVENTS.emit_signal('changed_current_station', id)


func get_current_station_id():
	return _current_station_id


func get_current_station():
	return RAIL_NETWORK.network.get(_current_station_id)


func get_has_arrived_by_left_connection():
	return _has_arrived_by_left_connection


func get_current_station_outgoing_connections():
	var data = get_current_station()
	if _has_arrived_by_left_connection:
		return data.connections.right
	else:
		return data.connections.left


func get_paths_count() -> int:
	return get_current_station_outgoing_connections().size()


func get_target_station_id():
	return _target_station_id


func get_target_station():
	return RAIL_NETWORK.network.get(_target_station_id)


func train_is_on_express() -> bool:
	return false # TODO


func set_selected_path(i: int):
	_selected_path = i
	var target_id = get_current_station_outgoing_connections()[i]
	_target_station_id = target_id
	
	EVENTS.emit_signal('select_junction', i)


func get_selected_path() -> int:
	return _selected_path


func set_train(id):
	_train = id
	EVENTS.emit_signal('set_train')


func get_train():
	return _train


func set_carriages(new_carriages):
	_carriages = new_carriages
	EVENTS.emit_signal('set_carriages')
	

func change_carriage(i, new_carriage):
	_carriages[i] = new_carriage
	EVENTS.emit_signal('set_carriages')


func get_carriages():
	return _carriages


func change_inventory(data):
	var found = false
	for i in _inventory.size():
		var item = _inventory[i]
		if item.id == data.id:
			item.count += data.count
			if item.count <= 0:
				_inventory.remove(i)
			found = true
			break

	if not found and data.count > 0:
		_inventory.append(data)

	EVENTS.emit_signal('inventory_changed')


func get_inventory():
	return _inventory.duplicate()


func change_money(diff: int):
	_money += diff
	EVENTS.emit_signal('money_changed')


func get_money() -> int:
	return _money


func get_passenger_amount() -> int:
	return _passengers


func set_passenger_amount(new_amount: int):
	_passengers = new_amount


func change_passenger_amount(change: int):
	_passengers += change


func get_passenger_capacity() -> int:
	var capacity = 0
	for carriage in _carriages:
		if DATA.CARRIAGES[carriage.id].is_passenger:
			capacity += DATA.CARRIAGES[carriage.id].capacity
	return capacity


func get_free_passenger_capacity() -> int:
	return get_passenger_capacity() - _passengers
