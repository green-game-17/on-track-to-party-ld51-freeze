extends Control


onready var button_box = $ButtonBox
onready var button_1 = $ButtonBox/Button1
onready var button_2 = $ButtonBox/Button2
onready var button_3 = $ButtonBox/Button3
onready var button_arr = [
	button_1, button_2, button_3
]
onready var go_box = $GoBox
onready var go_button = $GoBox/Button
onready var timer = $Timer


func _ready():
	EVENTS.connect('depart_station', self, '__on_depart')
	EVENTS.connect('arrive_station', self, '__on_arrive')

	button_1.connect('pressed', self, '__on_selected_path', [1])
	button_2.connect('pressed', self, '__on_selected_path', [2])
	button_3.connect('pressed', self, '__on_selected_path', [3])

	timer.connect('timeout', self, '__on_timeout')

	go_box.hide()

	__on_arrive()


func _input(_event):
	if not visible:
		return

	for i in range(1, 4):
		if Input.is_action_just_pressed('ui_%d'%i):
			__on_selected_path(i)


func __on_arrive():
	hide()


func __on_depart():
	show()
	
	var next_paths_count = STATE.get_paths_count()
	if next_paths_count > 1:
		button_box.show()
		for b in button_arr:
			b.disabled = false
		if STATE.get_paths_count() < 3:
			button_3.hide()
		else:
			button_3.show()

	timer.start()


func __on_timeout():
	for b in button_arr:
		b.disabled = true
	EVENTS.emit_signal('passed_junction')


func __on_selected_path(n):
	STATE.set_selected_path(n-1)

	button_1.pressed = false
	button_2.pressed = false
	button_3.pressed = false
	button_arr[n-1].pressed = true
