extends Control


signal count_changed(old_count, count)

onready var value_edit = $'%ValueEdit'


var _id = ID.ITEM_OPAL
var _cost = 0
var _prev_count = 0


func _ready():
	$'%PlusButton'.connect('pressed', self, '__on_button', [+1])
	$'%MinusButton'.connect('pressed', self, '__on_button', [-1])
	$'%ValueEdit'.connect('text_changed', self, '__on_value_edit_changed')


func set_data(id, count: int = 0, cost: int = 0):
	$'%Ressource'.set_data(id, 0, cost)

	$'%ValueEdit'.set_text('%d'%count)

	_id = id
	_cost = cost


func get_id():
	return _id


func set_count(count: int, no_signal: bool = false):
	value_edit.set_text('%d'%count)
	if not no_signal:
		emit_signal('count_changed', _prev_count, count)
	_prev_count = count


func get_count() -> int:
	return value_edit.get_text().to_int()


func set_cost(cost: int):
	$'%Ressource'.set_cost(cost)
	_cost = cost


func get_cost() -> int:
	return _cost


func __on_button(value: int):
	set_count(int(max(value_edit.get_text().to_int() + value, 0)))


func __on_value_edit_changed(value_str: String):
	set_count(value_str.to_int())
