extends Control


onready var title = $'%Title'

onready var cargo_tab = $'%CargoTab'
onready var passengers_tab = $'%PassengersTab'
onready var sell_tab = $'%SellTab'
onready var upgrades_tab = $'%UpgradesTab'
onready var cargo_view = $'%CargoView'
onready var passengers_view = $'%PassengersView'
onready var sell_view = $'%SellView'
onready var upgrades_view = $'%UpgradesView'
onready var tabs_views = {
	ID.TAB_CARGO: [cargo_tab, cargo_view],
	ID.TAB_PASSENGERS: [passengers_tab, passengers_view],
	ID.TAB_SELL: [sell_tab, sell_view],
	ID.TAB_UPGRADE: [upgrades_tab, upgrades_view]
}

# Cargo:
onready var no_cargo_label = $'%NoCargoLabel'
onready var cargo_container = $'%CargoContainer'
onready var cargo_card_1 = $'%CargoCard1'
onready var cargo_card_2 = $'%CargoCard2'
onready var cargo_card_3 = $'%CargoCard3'
onready var cargo_cards = [
	cargo_card_1, cargo_card_2, cargo_card_3
]
onready var total_cargo_cost_label = $'%TotalCargoCostLabel'
onready var total_cargo_weight_label = $'%TotalCargoWeightLabel'
onready var total_passenger_count_label = $'%TotalPassengerCountLabel'

# Passengers:
onready var no_passengers_label = $'%NoPassengersLabel'
onready var passengers_container = $'%PassengersContainer'
onready var passenger_card_1 = $'%PassengerCard1'
onready var passenger_card_2 = $'%PassengerCard2'
onready var passenger_card_3 = $'%PassengerCard3'
onready var passenger_cards = [
	passenger_card_1, passenger_card_2, passenger_card_3
]

# Sell:
onready var common_cargo_container = $'%CommonCargoContainer'
onready var specific_cargo_container = $'%SpecificCargoContainer'
var sell_item_entries = {}

# inventory:
onready var money_label = $'%MoneyLabel'
onready var inventory_items_container = $'%inventoryItemsContainer'
onready var weight_label = $'%WeightLabel'
onready var weight_progress_bar = $'%WeightProgressBar'


var ressource_card_scene = preload('res://components/hud/station/ressource_card.tscn')
var ressource_scene = preload('res://components/hud/station/ressource.tscn')

var station_data


func _ready():
	cargo_tab.connect('pressed', self, '__set_visible', [ID.TAB_CARGO])
	passengers_tab.connect('pressed', self, '__set_visible', [ID.TAB_PASSENGERS])
	sell_tab.connect('pressed', self, '__set_visible', [ID.TAB_SELL])
	upgrades_tab.connect('pressed', self, '__set_visible', [ID.TAB_UPGRADE])

	$'%CargoButton'.connect('pressed', self, '__on_cargo_commit')
	$'%PassengersButton'.connect('pressed', self, '__on_passengers_commit')
	$'%SellButton'.connect('pressed', self, '__on_sell_commit')
	$'%GoButton'.connect('pressed', self, '__on_pressed_go')

	EVENTS.connect('depart_station', self, '__on_depart_station')
	EVENTS.connect('arrive_station', self, '__on_arrive_station')
	EVENTS.connect('money_changed', self, '__on_money_changed')
	EVENTS.connect('inventory_changed', self, '__on_inventory_changed')
	EVENTS.connect('passengers_changed', self, '__on_passengers_changed')
	EVENTS.connect('set_carriages', self, '__on_set_train_composition')
	EVENTS.connect('set_train', self, '__on_set_train_composition')

	__set_visible(ID.TAB_CARGO)
	__on_arrive_station()
	__on_money_changed()
	__on_inventory_changed()
	__on_set_train_composition()

	cargo_card_1.connect('count_changed', self, '__on_cargo_card_count_changed', [cargo_card_1])
	cargo_card_2.connect('count_changed', self, '__on_cargo_card_count_changed', [cargo_card_1])
	cargo_card_3.connect('count_changed', self, '__on_cargo_card_count_changed', [cargo_card_1])

	while common_cargo_container.get_child_count() > 0:
		var c = common_cargo_container.get_child(0)
		common_cargo_container.remove_child(c)
		c.queue_free()
	while specific_cargo_container.get_child_count() > 0:
		var c = specific_cargo_container.get_child(0)
		specific_cargo_container.remove_child(c)
		c.queue_free()

	for key in DATA.ITEMS[ID.COMMON_ITEM]:
		var n = ressource_card_scene.instance()
		n.set_data(key, 0, 0)
		sell_item_entries[key] = n
		common_cargo_container.add_child(n)
		n.connect('count_changed', self, '__on_sell_card_count_changed', [n])
	for key in DATA.ITEMS[ID.SPECIAL_ITEM]:
		var n = ressource_card_scene.instance()
		n.set_data(key, 0, 0)
		sell_item_entries[key] = n
		specific_cargo_container.add_child(n)
		n.connect('count_changed', self, '__on_sell_card_count_changed', [n])


func _input(_event):
	if Input.is_action_just_pressed('ui_accept') and visible:
		__on_pressed_go()


func __on_pressed_go():
	EVENTS.emit_signal('depart_station')
		

func __set_visible(id):
	for tv in tabs_views.values():
		tv[0].modulate.a = 0.8
		tv[1].hide()
	
	var selected_tv = tabs_views[id]
	selected_tv[0].modulate.a = 1.0
	selected_tv[1].show()


func __on_depart_station():
	hide()


func __on_arrive_station():
	station_data = STATE.get_current_station()

	title.set_text(station_data.name)
	title.modulate = Color.goldenrod if station_data.is_main_station else Color.webgray
	print(station_data.is_main_station)

	if station_data.has_materials.size():
		cargo_container.show()
		no_cargo_label.hide()
		for i in station_data.has_materials.size():
			var item_id = station_data.has_materials[i]
			var item_data = DATA.ITEMS_DATA[item_id]
			cargo_cards[i].set_data(item_id, 0, item_data.costs)
		if station_data.has_materials.size() == 2:
			cargo_card_3.hide()
			cargo_card_3.set_count(0)
		else: 
			cargo_card_3.show()
	else:
		no_cargo_label.show()
		cargo_container.hide()

	# to do
	if false or station_data.is_passenger_station:
		STATE.change_money(STATE.get_passenger_amount() * 75)
		STATE.set_passenger_amount(0)
		passengers_container.show()
		no_passengers_label.hide()
		passenger_cards[0].set_data('somewhere')
	else:
		no_passengers_label.show()
		passengers_container.hide()

	for key in sell_item_entries.keys():
		var data = DATA.ITEMS_DATA[key]
		var value = data.value.same_biome if (station_data.biome in data.available_in) else data.value.different_biome
		sell_item_entries[key].set_cost(value)
		sell_item_entries[key].set_count(0)

	show()


func __on_cargo_commit():
	for i in station_data.has_materials.size():
		var count = cargo_cards[i].get_count()
		if count == 0:
			continue
		var id = station_data.has_materials[i]
		STATE.change_inventory({
			id = id,
			count = count
		})
		STATE.change_money(-1 * DATA.ITEMS_DATA[id].costs * count)
		cargo_cards[i].set_count(0)


func __on_passengers_changed():
	var count = 0
	for c in passenger_cards:
		count += c.get_count()
	if count > STATE.get_free_passenger_capacity():
		var diff = count - STATE.get_free_passenger_capacity()
		for c in passenger_cards:
			if c.get_count() >= diff:
				c.set_count(c.get_count() - diff)
				break
			elif c.get_count() > 0:
				diff -= c.get_count()
				c.set_count(0)
		count = STATE.get_free_passenger_capacity()


func __on_passengers_commit():
	var passengers = 0
	for c in passenger_cards:
		passengers += c.get_count()
	STATE.change_passenger_amount(passengers)
	total_passenger_count_label.set_text('%d'%[STATE.get_passenger_amount()])
	for c in passenger_cards:
		c.set_count(0)


func __on_sell_commit():
	for key in sell_item_entries.keys():
		var count = sell_item_entries[key].get_count()
		if count == 0:
			continue
		STATE.change_inventory({
			id = key,
			count = -1 * count
		})
		STATE.change_money(sell_item_entries[key].get_cost() * count)
		sell_item_entries[key].set_count(0)


func __on_money_changed():
	money_label.set_text('%d%s'%[ STATE.get_money(), DATA.CURRENCY_SYMBOL ])


func __on_inventory_changed():
	while inventory_items_container.get_child_count() > 0:
		var c = inventory_items_container.get_child(0)
		inventory_items_container.remove_child(c)
		c.queue_free()

	var total_weight = 0
	for item in STATE.get_inventory():
		var n = ressource_scene.instance()
		n.set_data(item.id, item.count, 0)
		inventory_items_container.add_child(n)
		var data = DATA.ITEMS_DATA[item.id]
		total_weight += item.count * data.weight

	var total_capacity = 0
	for carriage in STATE.get_carriages():
		var data = DATA.CARRIAGES[carriage.id]
		if data.type == ID.CARRIAGE_TYPE_CARGO:
			total_capacity += data.capacity

	weight_label.set_text('%dt'%total_weight)
	weight_progress_bar.max_value = total_capacity
	weight_progress_bar.set_value(total_weight)


func __on_cargo_card_count_changed(old_count, count, node):
	if count == 0:
		return

	var total_cost = 0
	for i in station_data.has_materials.size():
		var item_count = cargo_cards[i].get_count()
		if item_count == 0:
			continue
		var id = station_data.has_materials[i]
		total_cost += DATA.ITEMS_DATA[id].costs * item_count
	
	if total_cost > STATE.get_money():
		if abs(old_count - count) == 1.0:
			node.set_count(old_count, true)
		else:
			node.set_count(0, true)


func __on_sell_card_count_changed(old_count, count, node):
	if count == 0:
		return

	var id = node.get_id()

	var valid = false
	for item in STATE.get_inventory():
		if item.id == id:
			if count <= item.count:
				valid = true
				break

	if not valid:
		if abs(old_count - count) == 1.0:
			node.set_count(old_count, true)
		else:
			node.set_count(0, true)


func __on_set_train_composition():
	var data = STATE.get_carriages()

	var upgrade_scene = load('res://components/hud/station/upgrade_card.tscn')
	var entries_container = $'%UpgradesContainer'

	while entries_container.get_child_count() > 0:
		var c = entries_container.get_child(0)
		entries_container.remove_child(c)
		c.queue_free()

	for i in data.size():
		var nc = upgrade_scene.instance()
		nc.set_data(data[i].id, false)
		entries_container.add_child(nc)
		nc.connect('upgrade_pressed', self, '__on_upgrade_pressed_for_carriage', [i])

	var nlc = upgrade_scene.instance()
	nlc.set_data(STATE.get_train(), true)
	entries_container.add_child(nlc)
	nlc.connect('upgrade_pressed', self, '__on_upgrade_pressed_for_locomotice')

	var upgrade_entry_scene = load('res://components/hud/station/upgrade_entry.tscn')
	var ne = upgrade_entry_scene.instance()
	ne.set_data(ID.CARRIAGE_PARTY, true, false)
	entries_container.add_child(ne)
	ne.connect('upgrade_pressed', self, '__on_party')


func __on_upgrade_pressed_for_carriage(new_id, i):
	STATE.change_carriage(i, {id = new_id})


func __on_upgrade_pressed_for_locomotice(new_id):
	STATE.set_train(new_id)

func __on_party():
	$'%Party'.show()
