extends Control


signal upgrade_pressed()


func _ready():
	$'%UpgradeButton'.connect('pressed', self, '__on_upgrade_pressed')


func set_data(id, upgrade_visible: bool, is_locomotive: bool):
	var data
	if is_locomotive:
		data = DATA.TRAINS[id]
	else:
		data = DATA.CARRIAGES[id]

	var carriage_anchor = $'%CarriageAnchor'
	while carriage_anchor.get_child_count() > 0:
		var c = carriage_anchor.get_child(0)
		carriage_anchor.remove_child(c)
		c.queue_free()
	var nc = data.model.instance()
	carriage_anchor.add_child(nc)
	
	$'%TitleLabel'.set_text(data.name)
	$'%TierLabel'.set_text('Mk %d'%data.tier)
	if not is_locomotive and 'capacity' in data:
		$'%CapacityLabel'.set_text('%dt'%data.capacity)
	else:
		$'%CapacityLabel'.set_text('∞')

	var ressource_scene = load('res://components/hud/station/ressource_small.tscn')
	var costs_container = $'%CostsContainer'
	while costs_container.get_child_count() > 0:
		var c = costs_container.get_child(0)
		costs_container.remove_child(c)
		c.queue_free()
	for key in data.costs.keys():
		if key != ID.MONEY:
			var r = ressource_scene.instance()
			r.set_data(key, data.costs[key])
			costs_container.add_child(r)
	
	$'%MoneyCostLabel'.set_text(
		'%d%s'%[data.costs[ID.MONEY], DATA.CURRENCY_SYMBOL]
	)

	if upgrade_visible:
		$'%UpgradeButton'.show()
	else:
		$'%UpgradeButton'.hide()


func __on_upgrade_pressed():
	emit_signal('upgrade_pressed')
