extends Node


onready var wheel_joint_1 = $train/WheelJoint1
onready var wheel_joint_2 = $train/WheelJoint2
onready var wheel_joint_3 = $train/WheelJoint3
onready var wheel_joint_4 = $train/WheelJoint4

const rotate_step = -PI / 4


func _process(delta):
	if wheel_joint_1:
		__rotate(wheel_joint_1, delta)
	if wheel_joint_2:
		__rotate(wheel_joint_2, delta)
	if wheel_joint_3:
		__rotate(wheel_joint_3, delta)
	if wheel_joint_4:
		__rotate(wheel_joint_4, delta)


func __rotate(node, delta):
	var r = node.get_rotation()
	var new_r_z = r.z + rotate_step * delta
	if new_r_z > PI * 2:
		new_r_z -= PI * 2
	node.set_rotation(Vector3(r.x, r.y, new_r_z))
